let collection = [];

// Write the queue functions below.

// [1]
function print() {
    // It will show the array
    return collection;
 }

// [2-3]
function enqueue(element) {
    //In this funtion you are going to make an algo that will "add an element" to the array
    // Mimic the function of push method

    collection[collection.length] = element
    return collection;
 }

// [4-5-6]
function dequeue() {
    // In here you are going to "remove the last element" in the array
    for (let i = 1; i < collection.length; i++) {
        collection[i - 1] = collection[i];
        collection.length--;
    }
    return collection;
 }

// [7]
function front() {
    // In here, you are going to "get the first element"
    let first = collection[0];
    return first;
 }

// starting from here, di na pwede gumamit ng .length property
// [8]
function size() {
     // Number of elements   
     // *bawal .length
    let length = 0;
    for(let i = 0; collection[i] != null; i++){
        length = length + 1;
    }
    return length;
 }

// [9]
function isEmpty() {
    // it will check whether the function is empty or not
    // *pwede .length
    if(collection.length = null){
        return true;
    }
    return false;
 }

module.exports = {
    collection,
    print,
    enqueue,
    dequeue,
    front,
    size,
    isEmpty
};